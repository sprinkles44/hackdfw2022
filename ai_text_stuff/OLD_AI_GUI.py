import tkinter as tk
from xml.dom.expatbuilder import FragmentBuilder
import boto3
import string
import pandas as pd
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from datetime import datetime



global gui_keys

def key_in_phrase_batch(phrase_list: string, keyword_set: list) -> dict:
    # Check if keyword in each phrase
    keyword_phrase_dict = {}
    for i in range(len(phrase_list)):        
        clean_phrase = phrase_list[i].translate(str.maketrans('', '', string.punctuation))
        phrase_set = set(clean_phrase.lower().split())
        keywords_in_phrase = keyword_set.intersection(phrase_set)
        if keywords_in_phrase:
            for k in keywords_in_phrase:
                if k in keyword_phrase_dict.keys():
                    keyword_phrase_dict[k].append(i)
                else:
                    keyword_phrase_dict.update({k : [i]})
                    
    return keyword_phrase_dict

def stress_locations_and_levels(A): #A is an array of [DateTime, Location, SA1, SA2, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10]
    loc_stress_0=loc_stress_1=loc_stress_2=loc_stress_3=0
    count_0=count_1=count_2=count_3=0
    cols=2
    rows=len(A)
    for bussy in range(len(A)):
        if A.iloc[bussy,2] == 0:
            count_0=count_0+1
        elif A.iloc[bussy,2] == 1:
            count_1=count_1+1
        elif A.iloc[bussy,2] == 2:
            count_2=count_2+1
        elif A.iloc[bussy,2] == 3:
            count_3=count_3+1
        for tussy in range(5,len(A.iloc[bussy])):
            if A.iloc[bussy,2] == 0:
                loc_stress_0=loc_stress_0+A.iloc[bussy,tussy]
            elif A.iloc[bussy,2] == 1:
                loc_stress_1=loc_stress_1+A.iloc[bussy,tussy]
            elif A.iloc[bussy,2] == 2:
                loc_stress_2=loc_stress_2+A.iloc[bussy,tussy]
            elif A.iloc[bussy,2] == 3:
                loc_stress_3=loc_stress_3+A.iloc[bussy,tussy]
            #print(A.iloc[bussy,tussy])
    return (loc_stress_0/(count_0),loc_stress_1/(count_1),loc_stress_2/(count_2),loc_stress_3/(count_3)) #Returns average stress for Dallas, Fort Worth, San Antonio, Houston

def stress_over_time(A):
    Jan=Feb=Mar=Apr=May=Jun=Jul=Aug=Sep=Oct=Nov=Dec=pd.DataFrame()
    for rampart in range(len(A)):
        d=datetime.strptime(A.iloc[rampart,1],"%Y-%m-%dT%H:%M:%S.%fz")
        if d.month == 1:
            Jan=pd.concat([Jan,A.iloc[rampart]])
        elif d.month == 2:
            Feb=pd.concat([Feb,A.iloc[rampart]])
        elif d.month == 3:
            Mar=pd.concat([Mar,A.iloc[rampart]])
        elif d.month == 4:
            Apr=pd.concat([Apr,A.iloc[rampart]])
        elif d.month == 5:
            May=pd.concat([May,A.iloc[rampart]])
        elif d.month == 6:
            Jun=pd.concat([Jun,A.iloc[rampart]])
        if d.month == 7:
            Jul=pd.concat([Jul,A.iloc[rampart]])
        elif d.month == 8:
            Aug=pd.concat([Aug,A.iloc[rampart]])
        elif d.month == 9:
            Sep=pd.concat([Sep,A.iloc[rampart]])
        elif d.month == 10:
            Oct=pd.concat([Oct,A.iloc[rampart]])
        elif d.month == 11:
            Nov=pd.concat([Nov,A.iloc[rampart]])
        elif d.month == 12:
            Dec=pd.concat([Dec,A.iloc[rampart]])
    print(Jan)
    #print(pd.DataFrame(Jan,columns=['id','DateTime','Loc','SA1','SA2','Q1','Q2','Q3','Q4','Q5','Q6','Q7','Q8','Q9','Q10']))
        
    D_m1,FW_m1,SA_m1,H_m1=stress_locations_and_levels(Jan)
    '''
    D_m2,FW_m2,SA_m2,H_m2=stress_locations_and_levels(pd.DataFrame(Feb,columns=['']))
    D_m3,FW_m3,SA_m3,H_m3=stress_locations_and_levels(pd.DataFrame(Mar,columns=['']))
    D_m4,FW_m4,SA_m4,H_m4=stress_locations_and_levels(pd.DataFrame(Apr,columns=['']))
    D_m5,FW_m5,SA_m5,H_m5=stress_locations_and_levels(pd.DataFrame(May,columns=['']))
    D_m6,FW_m6,SA_m6,H_m6=stress_locations_and_levels(pd.DataFrame(Jun,columns=['']))
    D_m7,FW_m7,SA_m7,H_m7=stress_locations_and_levels(pd.DataFrame(Jul,columns=['']))
    D_m8,FW_m8,SA_m8,H_m8=stress_locations_and_levels(pd.DataFrame(Aug,columns=['']))
    D_m9,FW_m9,SA_m9,H_m9=stress_locations_and_levels(pd.DataFrame(Sep,columns=['']))
    D_m10,FW_m10,SA_m10,H_m10=stress_locations_and_levels(pd.DataFrame(Oct,columns=['']))
    D_m11,FW_m11,SA_m11,H_m11=stress_locations_and_levels(pd.DataFrame(Nov,columns=['']))
    D_m12,FW_m12,SA_m12,H_m12=stress_locations_and_levels(pd.DataFrame(Dec,columns=['']))
    D_mbm=[D_m1,D_m2,D_m3,D_m4,D_m5,D_m6,D_m7,D_m8,D_m9,D_m10,D_m11,D_m12] #Dallas Month by Month
    FW_mbm=[FW_m1,FW_m2,FW_m3,FW_m4,FW_m5,FW_m6,FW_m7,FW_m8,FW_m9,FW_m10,FW_m11,FW_m12]
    SA_mbm=[SA_m1,SA_m2,SA_m3,SA_m4,SA_m5,SA_m6,SA_m7,SA_m8,SA_m9,SA_m10,SA_m11,SA_m12]
    H_mbm=[H_m1,H_m2,H_m3,H_m4,H_m5,H_m6,H_m7,H_m8,H_m9,H_m10,H_m11,H_m12]
    return D_mbm,FW_mbm,SA_mbm,H_mbm
    '''
def create_pie_chart(fig,subplot,Values,Names):
    ax = fig.add_subplot(subplot) # add an Axes to the figure
    ax.pie(Values, radius=1, labels=Names,autopct='%0.2f%%')

def create_line_graph(fig,subplot,xlabel,ylabel,title,XY,*args,**kwargs):
    ax=fig.add_subplot(subplot)
    ax.plot(XY[0],XY[1],label=XY[2])
    for ar in args:
        ax.plot(ar[0],ar[1],label=ar[2])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.legend()
    
    

    

    


## Get Data from report.csv file
df = pd.read_csv('report.csv')

stress_points = []
stress_i_start = 5

Dallas_Stress,Fort_Worth_Stress,San_Antonio_Stress, Houston_Stress = stress_locations_and_levels(df) #returns averaged Percieved stress at addressed locations

n = len(df.columns)
for j in range(n):
    stress_questions = df.iloc[j, stress_i_start:stress_i_start+10]
    stress_point_list = [ int(point) for point in stress_questions] 
    stress_points.append( sum(stress_point_list) )

print(stress_points)

question_i = 3
data_read_batch = df.iloc[:, 3]


## Start Tkinter

root = tk.Tk()

keyword_entry = tk.Entry(root, width='50')
keyword_entry.pack()

comprehend = boto3.client(service_name='comprehend', region_name='us-east-1')

# Button to add Keyword
Lb1 = tk.Listbox(root)
Lb1.pack()

gui_keys = set([])

def key_button_pressed(read_data_batch):
    key = keyword_entry.get()
    if len(key) != 0 and len(gui_keys.intersection(set([key]))) == 0:
        Lb1.insert(0, key)
        gui_keys.add(key.lower())
        keyword_entry.delete(0,tk.END)

        sentiment_batch_output = comprehend.batch_detect_sentiment(TextList=read_data_batch, LanguageCode='en')

        print('\nSENTIMENT, Negative Score, Positive Score, Neutral Score, Mixed Score')
        print('-'*88)
        for i in range(len(read_data_batch)):

            phrase = sentiment_batch_output['ResultList'][i]

            sentiment = phrase['Sentiment']

            negative_score = phrase['SentimentScore']['Negative']
            positive_score = phrase['SentimentScore']['Positive']
            neutral_score = phrase['SentimentScore']['Neutral']
            mixed_score = phrase['SentimentScore']['Mixed']
            
            print(sentiment, negative_score, positive_score, neutral_score, mixed_score)


keyword_button = tk.Button(root, text='Add Keyword', command=lambda: key_button_pressed([""]))
keyword_button.pack()

#sentiment_batch_output = comprehend.batch_detect_sentiment(TextList=read_data_batch, LanguageCode='en')


#which_key_in_which_phrase = key_in_phrase_batch(read_data_batch, gui_keys)
#print(which_key_in_which_phrase)

# Read Keywords file
keywords_file = open('keywords.txt', 'r')
keywords = keywords_file.readlines()
keyword_list = [ word.lower().strip() for word in keywords ]
keyword_set = set(keyword_list)

root.title('Employee Moral Status Tool')
frameChartsLT = tk.Frame(root)
frameChartsLT.pack()

fig = Figure() # create a figure object

Subplot=211
Names=["BAD1","BAD2","BAD3"]
Values=[10,30,50]
create_pie_chart(fig,Subplot,Values,Names)
#create_pie_chart(fig,22,[123,456,789],["T1","T2","T3"])
#months=[1,2,3,4,5,6,7,8,9,10,11,12]
#D_mbm,FW_mbm,SA_mbm,H_mbm=stress_over_time(df)
#create_line_graph(fig,212,"Month","Percieved Stress","Percieved Stress Over time",[months,D_mbm,"Dallas"],[months,FW_mbm,"Fort Worth"],[months,SA_mbm,"San Antonio"],[months,H_mbm,"Houston"]) #[[X],[Y]] You may also add more than 1 set of data for stacked lines
chart1 = FigureCanvasTkAgg(fig,frameChartsLT)
chart1.get_tk_widget().pack()

root.mainloop()