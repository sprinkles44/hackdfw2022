from datetime import datetime
def stress_locations_and_levels(A): #A is an array of [DateTime, Location, SA1, SA2, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10]
    loc_stress_0=0
    loc_stress_1=0
    loc_stress_2=0
    loc_stress_3=0
    count_0=0
    count_1=0
    count_2=0
    count_3=0
    count=0
    cols=2
    rows=len(A)
    if type(A) is not type(["a","b","b"]):
        for bussy in range(len(A)):
            if int(A.iloc[bussy,2]) == 0:
                count_0 = count_0 + 1
            elif A.iloc[bussy,2] == 1:
                count_1=count_1+1
            elif A.iloc[bussy,2] == 2:
                count_2=count_2+1
            elif A.iloc[bussy,2] == 3:
                count_3=count_3+1
            for tussy in range(5,len(A.iloc[bussy])):
                if A.iloc[bussy,2] == 0:
                    loc_stress_0=loc_stress_0+A.iloc[bussy,tussy]
                elif A.iloc[bussy,2] == 1:
                    loc_stress_1=loc_stress_1+A.iloc[bussy,tussy]
                elif A.iloc[bussy,2] == 2:
                    loc_stress_2=loc_stress_2+A.iloc[bussy,tussy]
                elif A.iloc[bussy,2] == 3:
                    loc_stress_3=loc_stress_3+A.iloc[bussy,tussy]
    else:
        for bussy in range(len(A)):
            if A[bussy].iloc[2] == 0:
                count_0=count_0 + 1
                for tussy in range(5,len(A[bussy])): 
                    loc_stress_0=loc_stress_0 + A[bussy].iloc[tussy]
            if A[bussy].iloc[2] == 1:
                count_1=count_1 + 1
                for tussy in range(5,len(A[bussy])): 
                    loc_stress_1=loc_stress_1 + A[bussy].iloc[tussy]
            if A[bussy].iloc[2] == 2:
                count_2=count_2 + 1
                for tussy in range(5,len(A[bussy])): 
                    loc_stress_2=loc_stress_2 + A[bussy].iloc[tussy]
            if A[bussy].iloc[2] == 3:
                count_3=count_3 + 1
                for tussy in range(5,len(A[bussy])): 
                    loc_stress_3=loc_stress_3 + A[bussy].iloc[tussy]


    AA= 0 if count_0 < 1 else loc_stress_0/count_0
    BB= 0 if count_1 < 1 else loc_stress_1/count_1
    CC= 0 if count_2 < 1 else loc_stress_2/count_2
    DD= 0 if count_3 < 1 else loc_stress_3/count_3
    #Returns average stress for Dallas, Fort Worth, San Antonio, Houston
    return AA,BB,CC,DD

def stress_over_time(A):
    Jan=[]
    Feb=[]
    Mar=[]
    Apr=[]
    May=[]
    Jun=[]
    Jul=[]
    Aug=[]
    Sep=[]
    Oct=[]
    Nov=[]
    Dec=[]
    for rampart in range(len(A)):
        d=datetime.strptime(A.iloc[rampart,1],"%Y-%m-%dT%H:%M:%S.%fz")
        if d.month == 1:
            Jan.append(A.iloc[rampart])
        elif d.month == 2:
            Feb.append(A.iloc[rampart])
        elif d.month == 3:
            Mar.append(A.iloc[rampart])
        elif d.month == 4:
            Apr.append(A.iloc[rampart])
        elif d.month == 5:
            May.append(A.iloc[rampart])
        elif d.month == 6:
            Jun.append(A.iloc[rampart])
        elif d.month == 7:
            Jul.append(A.iloc[rampart])
        elif d.month == 8:
            Aug.append(A.iloc[rampart])
        elif d.month == 9:
            Sep.append(A.iloc[rampart])
        elif d.month == 10:
            Oct.append(A.iloc[rampart])
        elif d.month == 11:
            Nov.append(A.iloc[rampart])
        elif d.month == 12:
            Dec.append(A.iloc[rampart])
    #print(pd.DataFrame(Jan,columns=['id','DateTime','Loc','SA1','SA2','Q1','Q2','Q3','Q4','Q5','Q6','Q7','Q8','Q9','Q10']))
    #print("loc\t%d %d %d %d"%(loc_stress_0,loc_stress_1,loc_stress_2,loc_stress_3))
    #print("ccc\t%d %d %d %d"%(count_0,count_1,count_2,count_3))
    #print("%0.2f %0.2f %0.2f %0.2f"%(loc_stress_0/count_0,loc_stress_1/count_1,loc_stress_2/count_2,loc_stress_3/count_3))

    D_m1,FW_m1,SA_m1,H_m1=stress_locations_and_levels(Jan)
    D_m2,FW_m2,SA_m2,H_m2=stress_locations_and_levels(Feb)
    D_m3,FW_m3,SA_m3,H_m3=stress_locations_and_levels(Mar)
    D_m4,FW_m4,SA_m4,H_m4=stress_locations_and_levels(Apr)
    D_m5,FW_m5,SA_m5,H_m5=stress_locations_and_levels(May)
    D_m6,FW_m6,SA_m6,H_m6=stress_locations_and_levels(Jun)
    D_m7,FW_m7,SA_m7,H_m7=stress_locations_and_levels(Jul)
    D_m8,FW_m8,SA_m8,H_m8=stress_locations_and_levels(Aug)
    D_m9,FW_m9,SA_m9,H_m9=stress_locations_and_levels(Sep)
    D_m10,FW_m10,SA_m10,H_m10=stress_locations_and_levels(Oct)
    D_m11,FW_m11,SA_m11,H_m11=stress_locations_and_levels(Nov)
    D_m12,FW_m12,SA_m12,H_m12=stress_locations_and_levels(Dec)
    D_mbm=[D_m1,D_m2,D_m3,D_m4,D_m5,D_m6,D_m7,D_m8,D_m9,D_m10,D_m11,D_m12] #Dallas Month by Month
    FW_mbm=[FW_m1,FW_m2,FW_m3,FW_m4,FW_m5,FW_m6,FW_m7,FW_m8,FW_m9,FW_m10,FW_m11,FW_m12]
    SA_mbm=[SA_m1,SA_m2,SA_m3,SA_m4,SA_m5,SA_m6,SA_m7,SA_m8,SA_m9,SA_m10,SA_m11,SA_m12]
    H_mbm=[H_m1,H_m2,H_m3,H_m4,H_m5,H_m6,H_m7,H_m8,H_m9,H_m10,H_m11,H_m12]
    return D_mbm,FW_mbm,SA_mbm,H_mbm