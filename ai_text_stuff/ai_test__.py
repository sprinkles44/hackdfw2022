from urllib import response
import boto3
from pprint import pprint
import string
import logging
logger=logging.getLogger(__name__)
from botocore.exceptions import ClientError

# Single instance data
read_data = 'My manager stresses me out.'

# Batch data
read_data_batch = []
read_data_batch.append('The coworker next to me is always playing loud music and it is very annoying.')
read_data_batch.append('My manager stresses me out.')
read_data_batch.append('I really like my manager.')

# Read Keywords file
keywords_file = open('ai_text_stuff/keywords.txt', 'r')
keywords = keywords_file.readlines()
keyword_list = [ word.lower().strip() for word in keywords ]
keyword_set = set(keyword_list)

def key_in_phrase_batch(phrase_list: string, keyword_set: list) -> dict:
    # Check if keyword in phrase
    
    keyword_phrase_dict = {}

    for i in range(len(phrase_list)):
        
        clean_phrase = phrase_list[i].translate(str.maketrans('', '', string.punctuation))
        phrase_set = set(clean_phrase.lower().split())

        keywords_in_phrase = keyword_set.intersection(phrase_set)

        if keywords_in_phrase:
            for k in keywords_in_phrase:

                if k in keyword_phrase_dict.keys():
                    keyword_phrase_dict[k].append(i)
                else:
                    keyword_phrase_dict.update({k : [i]})
                    
    return keyword_phrase_dict





#gateway=ApiGatewayToService(boto3.client('apigateway'))
#client=boto3.client('appsync')
#print(gateway.api_id)
#boto3.get_api_cache()
print("AYO")
SS= boto3.client('amplify').get_backend_environment(appId='d14qpuyipbxax6',environmentName='ligma')
DB=boto3.resource('dynamodb', region_name="us-east-2")
tab=DB.Table('Entry-p5exq23h3rdpnkwvctalhhbz6u-ligma').get_item(Key={})
print(response['Item'])
#role = boto3.resource('iam').Role()
pprint( key_in_phrase_batch(read_data_batch, keyword_set) )
exit()

#===================================
# Real time single phrase processing

# print(read_data)
def key_in_phrase(phrase: string, keyword_set: list):
    # Check if keyword in phrase
    
    clean_phrase = phrase.translate(str.maketrans('', '', string.punctuation))
    
    phrase_set = set(clean_phrase.lower().split())
    print(phrase_set)

    keywords_in_phrase = keyword_set.intersection(phrase_set)
    if keywords_in_phrase:
        print(f'Phrase which contains keyword(s), {keywords_in_phrase}')
        print(read_data)

key_in_phrase(read_data, keyword_set)

comprehend = boto3.client(service_name='comprehend', region_name='us-east-1')

sentiment_output = comprehend.detect_sentiment(Text=read_data, LanguageCode='en')

# print(sentiment_output)
print(sentiment_output['Sentiment'])

negative_score = sentiment_output['SentimentScore']['Negative']
positive_score = sentiment_output['SentimentScore']['Positive']
neutral_score = sentiment_output['SentimentScore']['Neutral']
mixed_score = sentiment_output['SentimentScore']['Mixed']

print(negative_score, positive_score, neutral_score, mixed_score)




#=====================================
# Real Time Batch Processing (up to 25 documents)
# Make a list of phrases and submit batch

pprint(read_data_batch)

# uncomment for batch
# comprehend = boto3.client(service_name='comprehend', region_name='us-east-1')

sentiment_batch_output = comprehend.batch_detect_sentiment(TextList=read_data_batch, LanguageCode='en')

print('\nSENTIMENT, Negative Score, Positive Score, Neutral Score, Mixed Score')
print('-'*88)
for i in range(len(read_data_batch)):
    # print(sentiment_output)
    phrase = sentiment_batch_output['ResultList'][i]

    sentiment = phrase['Sentiment']

    negative_score = phrase['SentimentScore']['Negative']
    positive_score = phrase['SentimentScore']['Positive']
    neutral_score = phrase['SentimentScore']['Neutral']
    mixed_score = phrase['SentimentScore']['Mixed']
    
    print(sentiment, negative_score, positive_score, neutral_score, mixed_score)

#============================================
# Scheduling a job

# Create input and output folder for data

aws_output_filename = ''

s3 = boto3.client('s3')

# Specify parameters for the job
input_s3_url = 'https://s3.console.aws.amazon.com/s3/buckets/surveywebsite-storage-d6d3c5fa50816-ligma'
input_doc_format = 'ONE_DOC_PER_LINE'
output_s3_url = ''
data_access_role_arn = ''

# Job config
input_data_config = {'S3Uri': input_s3_url, 'InputFormat': input_doc_format}
output_data_config = {'S3Uri': output_s3_url}

# Start the client
comprehend = boto3.client(service_name='comprehend', region_name='us-east-1')

# Begin a job to detect the topics in the document collection
job_name = 'async test with s3'

start_job_sentiment = comprehend.start_sentiment_detection_job(
    InputDataConfig=input_data_config,
    OutputDataConfig=output_data_config,
    DataAccessRoleArn=data_access_role_arn,
    LanguageCode='en',
    JobName=job_name
)

job_id = start_job_sentiment['JobId']
print(f'Your sentiment detection jobID is: {job_id}')

