import tkinter as tk
from xml.dom.expatbuilder import FragmentBuilder
import boto3
import string
import pandas as pd
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from datetime import datetime
from methods import *



global gui_keys
global cut_phrase_list, keyword_set, keyword_phrase_dict, last_phrase_with_key

# Read Keywords file
keywords_file = open('keywords.txt', 'r')
keywords = keywords_file.readlines()
keyword_list = [ word.lower().strip() for word in keywords ]
keyword_set = set(keyword_list)
keyword_phrase_dict = {}
last_phrase_with_key = 0

def key_in_phrase_batch(phrase_list: list, keyword_set: set):
    # Check if keyword in each phrase
    for i in range(len(phrase_list)):        
        clean_phrase = phrase_list[i].translate(str.maketrans('', '', string.punctuation))
        phrase_set = set(clean_phrase.lower().split())
        keywords_in_phrase = keyword_set.intersection(phrase_set)
        if keywords_in_phrase:
            for k in keywords_in_phrase:
                if k in keyword_phrase_dict.keys():
                    keyword_phrase_dict[k].append(i)
                else:
                    keyword_phrase_dict.update({k : [i]})


def create_pie_chart(fig,subplot,Values,Names):
    ax = fig.add_subplot(subplot) # add an Axes to the figure
    ax.pie(Values, radius=1.3, labels=Names,autopct='%0.2f%%')


def create_line_graph(fig,subplot,xlabel,ylabel,title,XY,*args,**kwargs):
    ax=fig.add_subplot(subplot)
    ax.plot(XY[0],XY[1],label=XY[2])
    for ar in args:
        ax.plot(ar[0],ar[1],label=ar[2])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.legend()


def create_textlist(A):
    textList=[]
    for chevy in range(len(A)):
        textList.append(A.iloc[chevy,3])
        if len(textList)>24:
            break
    return textList


## Get Data from report.csv file
df = pd.read_csv('report.csv')

stress_points = []
stress_i_start = 5

#Dallas_Stress,Fort_Worth_Stress,San_Antonio_Stress, Houston_Stress = stress_locations_and_levels(df) #returns averaged Percieved stress at addressed locations

n = len(df.columns)
for j in range(n):
    stress_questions = df.iloc[j, stress_i_start:stress_i_start+10]
    stress_point_list = [ int(point) for point in stress_questions] 
    stress_points.append( sum(stress_point_list) )

print(stress_points)

question_i = 3
data_read_batch = df.iloc[:, 3]

TextList=create_textlist(df)

## Start Tkinter
root = tk.Tk()
root.geometry('1000x800')

keyword_entry = tk.Entry(root, width='50')
keyword_entry.pack()

comprehend = boto3.client(service_name='comprehend', region_name='us-east-1')

# Button to add Keyword
Lb1 = tk.Listbox(root)
Lb1.pack()


key_loc_in_phrase_dict ={}

phrase_list = df[df.columns[3]].tolist()
cut_phrase_list = phrase_list[0:24]
sentiment = []

sentiment_batch_output = comprehend.batch_detect_sentiment(TextList=cut_phrase_list, LanguageCode='en')

n_count = 0
p_count = 0
m_count = 0
neut_count = 0

p_example = 100
n_example = 100
m_example = 100
neut_example = 100

for i in range(len(cut_phrase_list)):
    phrase = sentiment_batch_output['ResultList'][i]
    sentiment_result = phrase['Sentiment']
    sentiment.append(sentiment_result)
    if sentiment_result.lower() == 'positive':
        p_count += 1
        if p_count == 1:
            p_example = i
    elif sentiment_result.lower() == 'negative':
        n_count += 1
        if n_count == 1:
            n_example = i
    elif sentiment_result.lower() == 'mixed':
        m_count += 1
        if m_count == 1:
            m_example = i
    else:
        neut_count += 1
        if neut_count == 1:
            neut_example = i
    

gui_keys = set([])
def key_button_pressed():
    key = keyword_entry.get()
    if len(key) != 0 and len(gui_keys.intersection(set([key]))) == 0:
        Lb1.insert(0, key)
        gui_keys.add(key.lower())
        keyword_entry.delete(0,tk.END)
        key_in_phrase_batch(cut_phrase_list, keyword_set)
        last_phrase_with_key = keyword_phrase_dict[keyword_list[0]]


keyword_button = tk.Button(root, text='Add Keyword', command=lambda: key_button_pressed())
keyword_button.pack()

if p_example != 100:
    p_example_text = f'Positive: {cut_phrase_list[p_example]}'
    p_example_phrase_label = tk.Label(root, text=p_example_text)
    p_example_phrase_label.pack()

if n_example != 100:
    n_example_text = f'Negative: {cut_phrase_list[n_example]}'
    n_example_phrase_label = tk.Label(root, text=n_example_text)
    n_example_phrase_label.pack()

if m_example != 100:
    m_example_text = f'Mixed: {cut_phrase_list[m_example]}'
    m_example_phrase_label = tk.Label(root, text=m_example_text)
    m_example_phrase_label.pack()

if neut_example != 100:
    neut_example_text = f'Neutral: {cut_phrase_list[neut_example]}'
    neut_example_phrase_label = tk.Label(root, text=neut_example_text)
    neut_example_phrase_label.pack()





root.title('SpeakUp - Employee Moral Status Tool')
frameChartsLT = tk.Frame(root)
frameChartsLT.pack()

fig = Figure() # create a figure object

Subplot=211
Names=["Postive","Negative","Neutral","Mixed"]
Values=[p_count,n_count,neut_count,m_count]
create_pie_chart(fig,Subplot,Values,Names)

#create_pie_chart(fig,22,[123,456,789],["T1","T2","T3"])
months=[1,2,3,4,5,6,7,8,9,10,11,12]
D_mbm,FW_mbm,SA_mbm,H_mbm=stress_over_time(df)
create_line_graph(fig,212,"Month","Percieved Stress","Percieved Stress Over time",[months,D_mbm,"Dallas"],[months,FW_mbm,"Fort Worth"],[months,SA_mbm,"San Antonio"],[months,H_mbm,"Houston"]) #[[X],[Y]] You may also add more than 1 set of data for stacked lines
chart1 = FigureCanvasTkAgg(fig,frameChartsLT)
chart1.get_tk_widget().pack()

root.mainloop()