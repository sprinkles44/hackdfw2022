export type AmplifyDependentResourcesAttributes = {
    "function": {
        "surveywebsite469139ba": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        }
    },
    "api": {
        "api81e92c17": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "surveywebsite": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    },
    "auth": {
        "surveywebsite": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "storage": {
        "s3surveywebsitestoraged6d3c5fa": {
            "BucketName": "string",
            "Region": "string"
        }
    }
}