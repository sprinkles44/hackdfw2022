//import logo from './logo.svg';
import './App.css'
import Amplify, { API } from 'aws-amplify'
import awsconfig from './aws-exports'
import React, { Component, useEffect, useState } from 'react'
import { DataStore } from '@aws-amplify/datastore';
import { Entry } from './models';
/*
class App extends Component{
  state={val:""}
};

handleChange=(e)=>{
  this.setState({val: e.target.value})
}
*/
const myAPI = "api81e92c17"
const path = '/entries'

const App = () => {
  const [message1,setMessage1] = useState("");
  const [message2,setMessage2] = useState("");
  const [input, setInput] = useState("");
  //const [input_SA1, setInput_SA1] = useState("")
  //const [input_SA2, setInput_SA2] = useState("")
  const [radio1, setRadio1] = useState("");
  const [radio2, setRadio2] = useState("");
  const [radio3, setRadio3] = useState("");
  const [radio4, setRadio4] = useState("");
  const [radio5, setRadio5] = useState("");
  const [radio6, setRadio6] = useState("");
  const [radio7, setRadio7] = useState("");
  const [radio8, setRadio8] = useState("");
  const [radio9, setRadio9] = useState("");
  const [radio10, setRadio10] = useState("");
/*
  const handleChange = event => {
    setMessage(event.target.value);
  };
*/
  //Function to fetch from our backend and update customers array
  /*
  function getEntry(e) {
    let entryId = e.input
    API.get(myAPI, path + "/" + entryId)
      .then(response => {
        console.log(response)
        let newEntry = [...entry]
        newEntry.push(response)
        setEntry(newEntry)

      })
      .catch(error => {
        console.log(error)
      })
  }
*/
  async function writeNewEntry(SA_1,SA_2,Q_1,Q_2,Q_3,Q_4,Q_5,Q_6,Q_7,Q_8,Q_9,Q_10){
    const d = new Date();
    await DataStore.save(
      new Entry({
      "DateTime": d.toISOString(),
      "SA1": SA_1,
      "SA2": SA_2,
      "Q1": parseInt(Q_1),
      "Q2": parseInt(Q_2),
      "Q3": parseInt(Q_3),
      "Q4": parseInt(Q_4),
      "Q5": parseInt(Q_5),
      "Q6": parseInt(Q_6),
      "Q7": parseInt(Q_7),
      "Q8": parseInt(Q_8),
      "Q9": parseInt(Q_9),
      "Q10": parseInt(Q_10)
    })
  );
  }

  return (

    <div className="App">
      <h1>Employee Satisfaction Survey</h1>
      <div>
        <div>
          <b>Q1:</b>
          <input type="radio" value="0" id="Q1" name="Q1" onChange={(e) => setRadio1(e.target.value)}/> 0
          <input type="radio" value="1" id="Q1" name="Q1" onChange={(e) => setRadio1(e.target.value)}/> 1
          <input type="radio" value="2" id="Q1" name="Q1" onChange={(e) => setRadio1(e.target.value)}/> 2
          <input type="radio" value="3" id="Q1" name="Q1" onChange={(e) => setRadio1(e.target.value)}/> 3
          <input type="radio" value="4" id="Q1" name="Q1" onChange={(e) => setRadio1(e.target.value)}/> 4

        </div>
        <div>
          <b>Q2:</b>
          <input type="radio" value="0" id="Q2" name="Q2" onChange={(e) => setRadio2(e.target.value)}/> 0
          <input type="radio" value="1" id="Q2" name="Q2" onChange={(e) => setRadio2(e.target.value)}/> 1
          <input type="radio" value="2" id="Q2" name="Q2" onChange={(e) => setRadio2(e.target.value)}/> 2
          <input type="radio" value="3" id="Q2" name="Q2" onChange={(e) => setRadio2(e.target.value)}/> 3
          <input type="radio" value="4" id="Q2" name="Q2" onChange={(e) => setRadio2(e.target.value)}/> 4

        </div>
        <div>
          <b>Q3:</b>
          <input type="radio" value="0" id="Q3" name="Q3" onChange={(e) => setRadio3(e.target.value)}/> 0
          <input type="radio" value="1" id="Q3" name="Q3" onChange={(e) => setRadio3(e.target.value)}/> 1
          <input type="radio" value="2" id="Q3" name="Q3" onChange={(e) => setRadio3(e.target.value)}/> 2
          <input type="radio" value="3" id="Q3" name="Q3" onChange={(e) => setRadio3(e.target.value)}/> 3
          <input type="radio" value="4" id="Q3" name="Q3" onChange={(e) => setRadio3(e.target.value)}/> 4

        </div>
        <div>
          <b>Q4:</b>
          <input type="radio" value="0" id="Q4" name="Q4" onChange={(e) => setRadio4(e.target.value)}/> 0
          <input type="radio" value="1" id="Q4" name="Q4" onChange={(e) => setRadio4(e.target.value)}/> 1
          <input type="radio" value="2" id="Q4" name="Q4" onChange={(e) => setRadio4(e.target.value)}/> 2
          <input type="radio" value="3" id="Q4" name="Q4" onChange={(e) => setRadio4(e.target.value)}/> 3
          <input type="radio" value="4" id="Q4" name="Q4" onChange={(e) => setRadio4(e.target.value)}/> 4

        </div>
        <div>
          <b>Q5:</b>
          <input type="radio" value="0" id="Q5" name="Q5" onChange={(e) => setRadio5(e.target.value)}/> 0
          <input type="radio" value="1" id="Q5" name="Q5" onChange={(e) => setRadio5(e.target.value)}/> 1
          <input type="radio" value="2" id="Q5" name="Q5" onChange={(e) => setRadio5(e.target.value)}/> 2
          <input type="radio" value="3" id="Q5" name="Q5" onChange={(e) => setRadio5(e.target.value)}/> 3
          <input type="radio" value="4" id="Q5" name="Q5" onChange={(e) => setRadio5(e.target.value)}/> 4

        </div>
        <div>
          <b>Q6:</b>
          <input type="radio" value="0" id="Q6" name="Q6" onChange={(e) => setRadio6(e.target.value)}/> 0
          <input type="radio" value="1" id="Q6" name="Q6" onChange={(e) => setRadio6(e.target.value)}/> 1
          <input type="radio" value="2" id="Q6" name="Q6" onChange={(e) => setRadio6(e.target.value)}/> 2
          <input type="radio" value="3" id="Q6" name="Q6" onChange={(e) => setRadio6(e.target.value)}/> 3
          <input type="radio" value="4" id="Q6" name="Q6" onChange={(e) => setRadio6(e.target.value)}/> 4

        </div>
        <div>
          <b>Q7:</b>
          <input type="radio" value="0" id="Q7" name="Q7" onChange={(e) => setRadio7(e.target.value)}/> 0
          <input type="radio" value="1" id="Q7" name="Q7" onChange={(e) => setRadio7(e.target.value)}/> 1
          <input type="radio" value="2" id="Q7" name="Q7" onChange={(e) => setRadio7(e.target.value)}/> 2
          <input type="radio" value="3" id="Q7" name="Q7" onChange={(e) => setRadio7(e.target.value)}/> 3
          <input type="radio" value="4" id="Q7" name="Q7" onChange={(e) => setRadio7(e.target.value)}/> 4
        </div>
        <div>
          <b>Q8:</b>
          <input type="radio" value="0" id="Q8" name="Q8" onChange={(e) => setRadio8(e.target.value)}/> 0
          <input type="radio" value="1" id="Q8" name="Q8" onChange={(e) => setRadio8(e.target.value)}/> 1
          <input type="radio" value="2" id="Q8" name="Q8" onChange={(e) => setRadio8(e.target.value)}/> 2
          <input type="radio" value="3" id="Q8" name="Q8" onChange={(e) => setRadio8(e.target.value)}/> 3
          <input type="radio" value="4" id="Q8" name="Q8" onChange={(e) => setRadio8(e.target.value)}/> 4
        </div>
        <div>
          <b>Q9:</b>
          <input type="radio" value="0" id="Q9" name="Q9" onChange={(e) => setRadio9(e.target.value)}/> 0
          <input type="radio" value="1" id="Q9" name="Q9" onChange={(e) => setRadio9(e.target.value)}/> 1
          <input type="radio" value="2" id="Q9" name="Q9" onChange={(e) => setRadio9(e.target.value)}/> 2
          <input type="radio" value="3" id="Q9" name="Q9" onChange={(e) => setRadio9(e.target.value)}/> 3
          <input type="radio" value="4" id="Q9" name="Q9" onChange={(e) => setRadio9(e.target.value)}/> 4
        </div>
        <div>
          <b>Q10:</b>
          <input type="radio" value="0" id="Q10" name="Q10" onChange={(e) => setRadio10(e.target.value)}/> 0
          <input type="radio" value="1" id="Q10" name="Q10" onChange={(e) => setRadio10(e.target.value)}/> 1
          <input type="radio" value="2" id="Q10" name="Q10" onChange={(e) => setRadio10(e.target.value)}/> 2
          <input type="radio" value="3" id="Q10" name="Q10" onChange={(e) => setRadio10(e.target.value)}/> 3
          <input type="radio" value="4" id="Q10" name="Q10" onChange={(e) => setRadio10(e.target.value)}/> 4

        </div>
        <b>Short Anwser 1:</b>
        <br></br>
        <input placeholder="Short Anwser 1" type="textarea" id="message1" value={message1} onChange={(e) => setMessage1(e.target.value)} />
      </div>
      <div>
        <b>Short Anwser 2:</b>
        <br></br>
        <input placeholder="Short Anwser 2" type="textarea" id="message2" value={message2} onChange={(e) => setMessage2(e.target.value)} />
      </div>
      <br />
      <button onClick={() => writeNewEntry(message1,message2,radio1,radio2,radio3,radio4,radio5,radio6,radio7,radio8,radio9,radio10)/*getEntry({ input })*/}>Submit</button>

     
    </div>
  )
}

export default App;

/*
 <h2 style={{ visibility: entry.length > 0 ? 'visible' : 'hidden' }}>Response</h2>
      {
        entry.map((thisEntry, index) => {
          return (
            <div key={thisEntry.entryId}>
              <span><b>EntryId:</b> {thisEntry.entryId} - <b>EntryName</b>: {thisEntry.entryName}</span>
            </div>)
        })
      }
*/

/*
import logo from './logo.svg';
import './App.css';
import Amplify, { API } from 'aws-amplify';
import React, { useEffect,useState } from 'react';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
*/