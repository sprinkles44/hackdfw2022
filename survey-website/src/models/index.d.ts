import { ModelInit, MutableModel } from "@aws-amplify/datastore";

type EntryMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

export declare class Entry {
  readonly id: string;
  readonly DateTime?: string | null;
  readonly SA1?: string | null;
  readonly SA2?: string | null;
  readonly Q1?: number | null;
  readonly Q2?: number | null;
  readonly Q3?: number | null;
  readonly Q4?: number | null;
  readonly Q5?: number | null;
  readonly Q6?: number | null;
  readonly Q7?: number | null;
  readonly Q8?: number | null;
  readonly Q9?: number | null;
  readonly Q10?: number | null;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
  constructor(init: ModelInit<Entry, EntryMetaData>);
  static copyOf(source: Entry, mutator: (draft: MutableModel<Entry, EntryMetaData>) => MutableModel<Entry, EntryMetaData> | void): Entry;
}